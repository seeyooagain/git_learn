### git 基本使用

#### git checkout

```
### 基于远端分支创建本地分支
git checkout -b your_branch_name origin/your_branch_name

### 基于当前分支创建本地分支
git checkout -b your_branch_name

### 切换分支
git checkout your_branch_name

### 切换到上一个分支
git checkout -

### 还原文件
git checkout -- file_name

```

#### git pull 拉取代码

其他开发人员在和你相同的分支上推送了代码 可以通过git pull拉取新提交的代码到本地

```
git pull
```

#### git add 添加文件到暂存区

```
## 添加当天目录下的所有文件
git add .

## 添加单个文件
git add file_name

## 添加多个文件
git add file_name1 file_name2
```

#### git commit 提交代码

```
## git add 之后还需要通过 git commit 提交代码 -m表示提交信息
git commit -m "提交信息"

## git commit -a 自动将所有修改过的文件添加到暂存区
git commit -a -m "提交信息"
```

#### git push 推送代码

把修改后的代码推送到远端仓库

```
git push origin master

git push origin your_branch_name
```

#### git merge 合并分支


## 合并分支
本地开发分支 local_branch1
远端开发分支 remote_branch2

当前分支 local_branch1 合并远端分支的代码 remote_branch2

git merge origin remote_branch2
